/**
 *
 */
package de.tm.za.util;

import java.awt.Component;
import java.awt.Point;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Faktory Klasse f�r das Handelsprogramm, um Hilfsmethoden zur Verf�gung zu
 * stellen.
 *
 * @author Elwashali
 * @created 13.01.2016
 *
 */
public class ServiceFactory {

	/**
	 * Ermittelt der {@link Point} der MittelBildschrim f�r das gegebenen
	 * {@link Component}.
	 *
	 * @author: Elwashali
	 * @created 13.01.2016
	 * @param pComponent,
	 *            die in der Mitte des Bildschirms angezeigt werden sollen.
	 * @return {@link Point}
	 */
	public static Point calculateScreenCenter(Component pComponent) {
		int x = (pComponent.getToolkit().getScreenSize().width - pComponent.getSize().width) / 2;
		int y = (pComponent.getToolkit().getScreenSize().height - pComponent.getSize().height) / 2;
		return new Point(x, y);
	}

	/**
	 * @param pString
	 * @return
	 */
	public static FileWriter createFileWriter(String pFileName) {
		FileWriter w = null;
		try {
			w = new FileWriter(pFileName);
			w.write("---------------------------------------------------------------------------------");
			w.write("| \tZeile |\talter Wert |\tneuer Wert | \tTick |\tBemerkung");
			w.write("---------------------------------------------------------------------------------");
			return w;
		} catch (IOException pE) {
			pE.printStackTrace();
		}
		return w;
	}

}
