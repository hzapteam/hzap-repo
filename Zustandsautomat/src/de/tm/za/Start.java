/**
 *
 */
package de.tm.za;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import de.tm.za.dto.ZustandautomatDTO;
import de.tm.za.gui.MainFrame;
import de.tm.za.gui.ZustandsautomatPresentationModel;
import de.tm.za.gui.ZustandsautomatView;
import de.tm.za.util.ServiceFactory;

/**
 * @author Elwashali
 *
 */
public class Start {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");

		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		MainFrame mainFrame = new de.tm.za.gui.MainFrame(new JFrame(),
				"Handelsprogramm - Zustandautomat GainCapital - Design: "
						+ UIManager.getLookAndFeel().getName());
		mainFrame.setPreferredSize(new Dimension(990, 500));

		ZustandsautomatPresentationModel model = new ZustandsautomatPresentationModel(
				new ZustandautomatDTO());
		ZustandsautomatView view = new ZustandsautomatView(model, mainFrame);

		mainFrame.getContentPane().add(view.buildPanel());
		mainFrame.pack();
		mainFrame.setVisible(true);
		mainFrame.setLocation(ServiceFactory.calculateScreenCenter(mainFrame));

	}

}
