/**
 *
 */
package de.tm.za.evaluation;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import de.tm.za.model.HmpObj;
import de.tm.za.model.TmpObj;
import de.tm.za.util.FileUtils;

/**
 * @author Elwashali
 * @created 19.01.2016
 *
 */
public class TmpHmpCalculation {

	/**
	 * presents the index values for the row and the column of ask value (column i)
	 **/
	public static final int[] START_ASK_INDEX = new int[] { 26, 9 };
	public static final int[] START_BID_INDEX = new int[] { 26, 10 };
	private int rowIndex = START_ASK_INDEX[0] - 1;
	private int columnIndexAsk = START_ASK_INDEX[1] - 1;
	private int columnIndexBid = START_BID_INDEX[1] - 1;

	private List<String[]> matrixData;
	FileWriter writer;
	private BigDecimal tmpNeu = new BigDecimal("0");
	private BigDecimal hmpNeu = new BigDecimal("0");
	private String tick;
	private int diff = 0;
	private BigDecimal startkurs = new BigDecimal("0");
	private BigDecimal oldValue = new BigDecimal("0");
	private boolean sucheTmp = false;
	private boolean sucheHmp = false;

	private int diffmax = 0;
	private int diffHmpMax = 0;
	private String tickAlt = "";
	private String tmp_tWert;
	private String hmp_tWert;
	private int countRowBlank = 0;
	private Object[] tmps = new Object[2];
	private Object[] hmps = new Object[2];
	private String kursrichtung;
	private int ticks = 0;
	private List<BigDecimal> switchPunkte = new ArrayList<BigDecimal>();
	private boolean ask, bid;

	public TmpHmpCalculation(List<String[]> pMatrixData, boolean pAsk, boolean pBid) throws IOException {
		this.matrixData = pMatrixData;
		this.ask = pAsk;
		this.bid = pBid;

	}

	/**
	 * @return
	 */
	private String createFileName() {
		// String pfad = "C:\\Users\\Administrator\\Desktop\\Pr�fung\\output.csv";

		String pfad = "C:\\Users\\Abdullatif\\Desktop\\Pr�fung\\";
		String fileName = StringUtils.EMPTY;
		if (this.ask && this.bid) {
			fileName = "output_all.csv";
		}
		if (this.ask) {
			fileName = "output_ask.csv";
		} else if (this.bid) {
			fileName = "output_bid.csv";
		}

		fileName = FileUtils.extendFilenameWithTimestamp(pfad + fileName);

		return fileName;
	}

	public boolean readMatrix(List<String[]> pMatrixData) throws IOException {

		boolean finish = false;

		this.writer = createFileWriter();

		initData(this.rowIndex, this.columnIndexAsk, this.columnIndexBid);

		for (int i = this.rowIndex; i < pMatrixData.size(); i++) {
			String[] zeile = pMatrixData.get(i);
			processor(zeile, i);

		}

		this.writer.close();
		finish = true;
		System.out.println("Anzahl alle Zeilen = " + pMatrixData.size());
		System.out.println("Anzahl leere Zeilen = " + this.countRowBlank);
		System.out.println("Anzahl bearbeitete Zeilen: " + (pMatrixData.size() - this.countRowBlank));
		System.out.println();
		String message = "";
		if (this.ask) {
			message = "Ask";
		} else {
			message = "Bid";
		}
		System.out.println("****************** " + message + " *********************");
		System.out.println("TMP1: " + ((TmpObj) this.tmps[0]).getTmp());
		System.out.println("TMP2: " + ((TmpObj) this.tmps[1]).getTmp());
		System.out.println();
		System.out.println("HMP1: " + ((HmpObj) this.hmps[0]).getHmp());
		System.out.println("HMP2: " + ((HmpObj) this.hmps[1]).getHmp());
		System.out.println("************************************************");

		return finish;
	}

	/**
	 * @param pZeile
	 * @param pI
	 * @throws IOException
	 */
	private void processor(String[] pZeile, int pIndex) throws IOException {
		String newValue = null;
		if (this.ask) {
			newValue = pZeile[this.columnIndexAsk];
		} else if (this.bid) {
			newValue = pZeile[this.columnIndexBid];
		}
		BigDecimal neuerWert = null;

		// Ist das Feld belegt sonst zum naechsten Feld
		if (StringUtils.isNotBlank(newValue)) {

			this.writer.write("\n\tZ" + getZeileNr(pIndex) + ";" + pZeile[2] + ";");
			try {
				neuerWert = validateNewValue(newValue);
			} catch (NumberFormatException pE) {
				this.countRowBlank++;
				return;
			}

			if (neuerWert != null) {
				this.writer.write(this.oldValue.intValue() + ";" + neuerWert.intValue() + ";");

				if (this.sucheTmp) {
					tmpBerechnen(neuerWert, getZeileNr(pIndex));
				} else if (this.sucheHmp) {
					hmpBerechnen(neuerWert, getZeileNr(pIndex));
				}

			}

		} else {
			this.countRowBlank++;
		}

	}

	/**
	 * Switchmesspuenkte-Berechnung. Ist eine Switchpunkt gefunden, wird er in der Liste der SwichtPunkte hinzugef�gt.
	 *
	 * @param pZeile
	 *            der Datensatz
	 * @throws IOException
	 *
	 */
	private void berechneSwitchmesspunkte(int pZeile) throws IOException {

		if (this.tmps[0] != null && this.hmps[0] != null) {
			BigDecimal tmp2 = null;
			if (this.tmps[0] != null && this.tmps[1] != null) {
				tmp2 = ((TmpObj) this.tmps[1]).getTmp();
				String tw1 = ((TmpObj) this.tmps[0]).gettWert();
				String tw2 = ((TmpObj) this.tmps[1]).gettWert();
				if (StringUtils.isEmpty(tw2) && StringUtils.isEmpty(tw1)) {
					int tmpTwert1 = Integer.valueOf(tw1.substring(2));
					int tmpTwert2 = Integer.valueOf(tw2.substring(2));

					if (tmpTwert1 >= tmpTwert2) {
						this.kursrichtung = Kursrichtung.SWITCH_TO_LONG.name();
						this.switchPunkte.add(tmp2);
						this.writer.write(" switch to Long ;;");
					}

					if (tmpTwert1 <= tmpTwert2) {
						this.kursrichtung = Kursrichtung.SWITCH_TO_SHORT.name();
						this.switchPunkte.add(tmp2);
						this.writer.write(" switch to Short ;;");
					}
				}
			}
			BigDecimal hmp2 = null;
			if (this.hmps[0] != null && this.hmps[1] != null) {
				int hmpTwert1 = Integer.valueOf(((HmpObj) this.hmps[0]).gettWert().substring(1));
				int hmpTwert2 = Integer.valueOf(((HmpObj) this.hmps[1]).gettWert().substring(1));
				hmp2 = ((HmpObj) this.hmps[1]).getHmp();

				if (hmpTwert1 <= hmpTwert2) {
					// long
					this.kursrichtung = Kursrichtung.SWITCH_TO_LONG.name();
					this.switchPunkte.add(hmp2);
					this.writer.write(" switch to Long ;;");
				}
				if (hmpTwert2 <= hmpTwert1) {
					this.kursrichtung = Kursrichtung.SWITCH_TO_SHORT.name();
					this.switchPunkte.add(hmp2);
					this.writer.write(" switch to short ;;");
				}

			}
		}
	}

	@SuppressWarnings("unused")
	private void switchKursrichtung(int pZeile) throws IOException {
		boolean hLong = false;
		boolean tLong = false;
		boolean hShort = false;
		boolean tShort = false;
		String startkurs = this.kursrichtung;

		if (this.tmps[0] != null && this.hmps[0] != null) {
			BigDecimal tmp2 = null;
			if (this.tmps[0] != null && this.tmps[1] != null) {
				tmp2 = ((TmpObj) this.tmps[1]).getTmp();
				int tmpTwert1 = Integer.valueOf(((TmpObj) this.tmps[0]).gettWert().substring(2));
				int tmpTwert2 = Integer.valueOf(((TmpObj) this.tmps[1]).gettWert().substring(2));

				if (this.kursrichtung.equals(Kursrichtung.SHORT.name())) {
					if (tmpTwert1 >= tmpTwert2) {
						// long
						tLong = true;
						this.kursrichtung = Kursrichtung.SWITCH_TO_LONG.name();

					}
				} else if (this.kursrichtung.equals(Kursrichtung.LONG.name())) {
					if (tmpTwert1 <= tmpTwert2) {
						tShort = true;
						this.kursrichtung = Kursrichtung.SWITCH_TO_SHORT.name();
					}
				}

			}
			BigDecimal hmp2 = null;
			if (this.hmps[0] != null && this.hmps[1] != null) {
				int hmpTwert1 = Integer.valueOf(((HmpObj) this.hmps[0]).gettWert().substring(1));
				int hmpTwert2 = Integer.valueOf(((HmpObj) this.hmps[1]).gettWert().substring(1));
				hmp2 = ((HmpObj) this.hmps[1]).getHmp();

				if (this.kursrichtung.equals(Kursrichtung.SHORT.name())) {
					if (hmpTwert1 <= hmpTwert2) {
						// long
						hLong = true;
						this.kursrichtung = Kursrichtung.SWITCH_TO_LONG.name();
					}
				} else if (this.kursrichtung.equals(Kursrichtung.LONG.name())) {
					if (hmpTwert2 <= hmpTwert1) {
						hShort = true;
						this.kursrichtung = Kursrichtung.SWITCH_TO_SHORT.name();
					}

				}
			}

			this.kursrichtung = startkurs;

			String str = "";
			if (this.sucheTmp) {
				str = "HMP: " + hmp2;
			} else {
				str = "TMP: " + tmp2;
			}

			if (tLong && hLong) {
				// Switch to long
				// kursrichtung = Kursrichtung.LONG.name();

				this.writer.write("\n; switch to Long ;;");

			} else if (tShort && hShort) {
				// switch to shot
				this.writer.write("\n; switch to Short ;;");
				// kursrichtung = Kursrichtung.SHORT.name();

			}
		}

	}

	/**
	 * @param pNeuerWert
	 * @throws IOException
	 */
	private void hmpBerechnen(BigDecimal pNeuerWert, int pZeile) throws IOException {
		computeTick(pNeuerWert, this.oldValue);
		this.writer.write(" H-Phase ;");

		if (pZeile == 596) {
			System.out.println();
		}

		this.diff = this.hmpNeu.subtract(pNeuerWert).intValue();
		// Hochphase steigend +
		this.diff = getAbsValue(this.diff);
		if (this.diff >= 3) {
			if (pNeuerWert.intValue() > this.hmpNeu.intValue()) {
				this.hmpNeu = pNeuerWert;
				this.writer.write("  merke hmp " + this.hmpNeu);
			} else if (isTickFallend(this.tick)) {
				this.ticks += Integer.valueOf(this.tick);
			} else if (isTickSteigend(this.tick)) {
				this.ticks = 0;
			}
		} else if (isTickSteigend(this.tick)) {

			if (this.diffHmpMax < (pNeuerWert.subtract(((TmpObj) this.tmps[1]).getTmp()).intValue())) {
				this.hmpNeu = pNeuerWert;
				this.writer.write("  ..merke hmp " + this.hmpNeu);
			}

			if (pNeuerWert.intValue() < this.hmpNeu.intValue()) { // und Tick steigend
				this.ticks = 0;
			}
		} else if (isTickFallend(this.tick)) {
			this.ticks += Integer.valueOf(this.tick);
		}

		if (this.diff >= 3 && isTickFallend(this.tick) && this.ticks <= -3) {

			// HMP setzen
			setzeHMP(pZeile);

			this.sucheHmp = false;
			this.sucheTmp = true;
			this.writer.write(" # HMP setzen: " + this.hmpNeu + ";; ### T-Wert = " + this.hmp_tWert + ";;");
			this.ticks = 0;
			this.oldValue = pNeuerWert;
			// Fehler z544 Testergebnis am 5.2.2016
			this.tmpNeu = pNeuerWert;
			return;

		}

		this.oldValue = pNeuerWert;

		int max = pNeuerWert.subtract(((TmpObj) this.tmps[1]).getTmp()).intValue();

		this.diffHmpMax = max > this.diffHmpMax ? max : this.diffHmpMax;

		this.tickAlt = this.tick;

	}

	/**
	 * @throws IOException
	 *
	 */
	private void setzeHMP(int pZeile) throws IOException {

		this.startkurs = this.hmpNeu;
		this.diffHmpMax = this.hmpNeu.subtract(((TmpObj) this.tmps[1]).getTmp()).intValue();
		this.hmp_tWert = "T" + (this.hmpNeu.subtract(((TmpObj) this.tmps[1]).getTmp()).intValue());

		HmpObj hmpObj = new HmpObj();
		hmpObj.setHmp(this.hmpNeu);
		hmpObj.setStartwert(this.startkurs);
		hmpObj.settWert(this.hmp_tWert);
		addHmp(hmpObj);
		berechneSwitchmesspunkte(pZeile);

	}

	/**
	 * @param pHmpObj
	 */
	private void addHmp(HmpObj pHmpObj) {
		this.hmps[0] = this.hmps[1];
		this.hmps[1] = pHmpObj;

	}

	/**
	 * @param pNeuerWert
	 * @throws IOException
	 *
	 */
	private void tmpBerechnen(BigDecimal pNeuerWert, int pZeile) throws IOException {
		computeTick(pNeuerWert, this.oldValue);
		this.writer.write(" T-Phase ;");
		this.diff = this.startkurs.subtract(pNeuerWert).intValue();

		if (pZeile == 685) {
			System.out.println();
		}

		if (this.diff >= 3 && isTickFallend(this.tick)) {
			this.tmpNeu = pNeuerWert;
			this.ticks = 0;
			this.writer.write(" merken tmp: " + this.tmpNeu);
		} else if (isTickFallend(this.tick)) {
			if (this.diffmax < this.diff && isTickFallend(this.tickAlt)) {
				this.tmpNeu = pNeuerWert;
				this.writer.write("..merke: " + this.tmpNeu);
			}
			this.ticks = 0;
		} else if (isTickSteigend(this.tick)) {
			this.ticks += Integer.valueOf(this.tick);
		}

		if (isTickSteigend(this.tick) && (Integer.valueOf(this.tick) >= 3 || this.ticks >= 3)) {

			// TMP setzen
			setzeTMP(pZeile);

			// this.diffmax = this.tmp.subtract(this.hmpNeu).intValue();
			this.sucheTmp = false;
			this.sucheHmp = true;
			this.writer.write(" # TMP setzen: " + this.tmpNeu + ";; ### T-Wert: " + this.tmp_tWert + ";;");
			this.ticks = 0;
			this.oldValue = pNeuerWert;
			// Fehler z544 Testergebnis am 5.2.2016
			this.hmpNeu = pNeuerWert;
			return;

		}

		this.oldValue = pNeuerWert;

		if (isDiffMax(this.diff)) {
			this.diffmax = this.diff;
		}
		this.tickAlt = this.tick;

	}

	/**
	 * @throws IOException
	 *
	 */
	private void setzeTMP(int pZeile) throws IOException {
		// this.tmp = this.tmpNeu;
		this.hmpNeu = this.tmpNeu;
		this.tmp_tWert = "T" + (this.tmpNeu.subtract(this.startkurs).intValue());
		this.startkurs = this.tmpNeu;

		TmpObj tmpObj = new TmpObj();
		tmpObj.setTmp(this.tmpNeu);
		tmpObj.setStartwert(this.startkurs);
		tmpObj.settWert(this.tmp_tWert);
		addTmp(tmpObj);
		berechneSwitchmesspunkte(pZeile);

	}

	/**
	 * @param pTmpObj
	 *
	 */
	private void addTmp(TmpObj pTmpObj) {
		this.tmps[0] = this.tmps[1];
		this.tmps[1] = pTmpObj;

	}

	/**
	 * @param pNewValue
	 * @return
	 */
	private BigDecimal validateNewValue(String pNewValue) {
		// T-Punkt loeschen
		String value = StringUtils.replaceChars(pNewValue, ".", "");

		// Laenge der Wert pruefen
		int length = value.length();

		if (length < 6) {
			int rest = 6 - length;
			while (rest > 0) {
				value = value + "0";
				rest--;
			}
		}

		return new BigDecimal(value);

	}

	/**
	 * @param pTick
	 * @return
	 */
	private boolean isTickSteigend(String pTick) {
		return Integer.valueOf(pTick) > 0;
	}

	/**
	 * @param pDiff
	 * @return
	 */
	private boolean isDiffMax(int pDiff) {
		return pDiff > this.diffmax;
	}

	/**
	 * @param pTick
	 * @return
	 */
	private boolean isTickFallend(String pTick) {
		return pTick.startsWith("-");
	}

	/**
	 * @param pRowIndex
	 * @return
	 */
	private int getZeileNr(int pRowIndex) {

		return (pRowIndex + 1);
	}

	/**
	 * @param pNewValue
	 * @param pOldValue
	 * @return
	 * @throws IOException
	 */
	private void computeTick(BigDecimal pNewValue, BigDecimal pOldValue) throws IOException {
		if (pOldValue != null && pNewValue != null) {
			if (pOldValue.compareTo(pNewValue) == 0) {
				this.tick = String.valueOf(0);
			} else if (pOldValue.compareTo(pNewValue) == -1) {
				this.tick = String.valueOf(pNewValue.subtract(pOldValue).intValue());
			} else if (pOldValue.compareTo(pNewValue) == 1) {
				this.tick = String.valueOf(pNewValue.subtract(pOldValue).intValue());
			}

			this.writer.write(this.tick + ";");
		}
	}

	/**
	 * Initials the values to start the calculation of the matrix data.
	 *
	 * @throws IOException
	 *
	 */
	private void initData(int pRowIndex, int pAskIndex, int pBidIndex) throws IOException {

		if (this.matrixData != null) {
			if (this.matrixData.get(pRowIndex).length > 0) {
				String value = null;
				if (this.ask) {
					value = this.matrixData.get(pRowIndex)[pAskIndex];
					System.out.println("====> Start Zeile: " + pRowIndex + " Startkurs = " + value);

				} else if (this.bid) {
					value = this.matrixData.get(pRowIndex)[pBidIndex];
				}
				if (value != null) {
					setStartkurs(value);
					this.tmpNeu = this.startkurs;
					this.oldValue = this.startkurs;
					this.hmpNeu = this.startkurs;
					this.sucheTmp = true;
				}
			}
		}

	}

	/**
	 * @return the matrixData
	 */
	public List<String[]> getMatrixData() {
		return this.matrixData;
	}

	/**
	 * @param pMatrixData
	 *            the matrixData to set
	 */
	public void setMatrixData(List<String[]> pMatrixData) {
		this.matrixData = pMatrixData;
	}

	/**
	 * @param pStartkurs
	 *            the startkurs to set
	 */
	public void setStartkurs(BigDecimal pStartkurs) {
		this.startkurs = pStartkurs;
	}

	/**
	 * @param pStartkurs
	 *            the startkurs to set
	 */
	public void setStartkurs(String pStartkurs) {

		try {
			if (StringUtils.isNotBlank(pStartkurs)) {
				String val = StringUtils.replaceChars(pStartkurs, ".", "");
				this.startkurs = new BigDecimal(val);
			}
		} catch (NumberFormatException pE) {
			pE.printStackTrace();
		}
	}

	/**
	 * @param pString
	 * @return
	 */
	public FileWriter createFileWriter() {
		String fileName = createFileName();
		FileWriter w = null;
		try {
			w = new FileWriter(fileName);
			w.write("; Ermittlung von TMP und HMP " + new Date() + ";");
			w.write("\n Zeile;Kursrichtung;alter Wert; neuer Wert;Tick;Phase;Bemerkung;");
			return w;
		} catch (IOException pE) {
			pE.printStackTrace();
		}
		return w;
	}

	private int getAbsValue(int pValue) {
		if (pValue < NumberUtils.INTEGER_ZERO) {
			return -pValue;
		}
		return pValue;
	}

}
