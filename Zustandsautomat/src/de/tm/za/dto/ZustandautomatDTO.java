/**
 * 
 */
package de.tm.za.dto;

import java.math.BigDecimal;

import com.jgoodies.binding.beans.Model;

/**
 * Data transfir object for the input MaskS
 * 
 * @author Elwashali
 * @created 11.01.2016
 * 
 */
public class ZustandautomatDTO extends Model {

	public static final String markt_property = "markt";
	public static final String kontoA_property = "kontoA";
	public static final String kontoB_property = "kontoB";
	public static final String startEquity_property = "startEquity";
	public static final String diffMP_property = "diffMP";
	public static final String notSwitch_property = "notSwitch";
	public static final String capitalOfStartEquityInPercent_property = "capitalOfStartEquityInPercent";
	public static final String switchDistance_property = "switchDistance";
	public static final String startSwitch_property = "startSwitch";
	public static final String relation_property = "relation";
	public static final String diffMACD_property = "diffMACD";
	public static final String vorsprungHP_property = "vorsprungHP";
	public static final String startKontrakt_property = "startKontrakt";
	public static final String zukauf_property = "zukauf";
	public static final String gewinnMitnahme_property = "gewinnMitnahme";
	public static final String amplitudenMessung_property = "amplitudenMessung";
	public static final String randMessung_property = "randMessung";
	public static final String relGewZuk_property = "relGewZuk";
	public static final String umschichtung_property = "umschichtung";
	public static final String hoehenmessung_property = "hoehenmessung";
	public static final String zukaufVirtuell_property = "zukaufVirtuell";

	/** serial version UID **/
	private static final long serialVersionUID = -3327985970290127710L;
	private String markt;
	private String kontoA;
	private String kontoB;

	private BigDecimal startEquity = new BigDecimal("5000");
	private BigDecimal diffMP = new BigDecimal("1");
	private BigDecimal notSwitch = new BigDecimal("1000");
	private BigDecimal capitalOfStartEquityInPercent = new BigDecimal("75");
	private BigDecimal switchDistance = new BigDecimal("10");
	private BigDecimal startSwitch = new BigDecimal("1000");
	private BigDecimal relation = new BigDecimal("25");
	private BigDecimal diffMACD = new BigDecimal("40");
	private BigDecimal vorsprungHP = new BigDecimal("0");
	private BigDecimal startKontrakt = new BigDecimal("20");
	private BigDecimal zukauf = new BigDecimal("25");
	private BigDecimal gewinnMitnahme = new BigDecimal("1000");
	private BigDecimal amplitudenMessung = new BigDecimal("40");
	private BigDecimal randMessung = new BigDecimal("25");
	private BigDecimal relGewZuk = new BigDecimal("25");
	private BigDecimal umschichtung = new BigDecimal("20");
	private BigDecimal hoehenmessung = new BigDecimal("20");
	private BigDecimal zukaufVirtuell = new BigDecimal("100");

	/**
	 * @return the markt
	 */
	public String getMarkt() {
		return markt;
	}

	/**
	 * @param pMarkt
	 *            the markt to set
	 */
	public void setMarkt(String pMarkt) {
		String oldValue = this.markt;
		this.markt = pMarkt;
		firePropertyChange(markt_property, oldValue, pMarkt);
	}

	/**
	 * @return the kontoA
	 */
	public String getKontoA() {
		return kontoA;
	}

	/**
	 * @param pKontoA
	 *            the kontoA to set
	 */
	public void setKontoA(String pKontoA) {
		String oldValue = this.kontoA;
		this.kontoA = pKontoA;
		firePropertyChange(kontoA_property, oldValue, pKontoA);
	}

	/**
	 * @return the kontoB
	 */
	public String getKontoB() {
		return kontoB;
	}

	/**
	 * @param pKontoB
	 *            the kontoB to set
	 */
	public void setKontoB(String pKontoB) {
		String oldValue = this.kontoB;
		this.kontoB = pKontoB;
		firePropertyChange(kontoB_property, oldValue, pKontoB);
	}

	/**
	 * @return the startEquity
	 */
	public BigDecimal getStartEquity() {
		return startEquity;
	}

	/**
	 * @param pStartEquity
	 *            the startEquity to set
	 */
	public void setStartEquity(BigDecimal pStartEquity) {
		BigDecimal oldValue = this.startEquity;
		this.startEquity = pStartEquity;
		firePropertyChange(startEquity_property, oldValue, pStartEquity);
	}

	/**
	 * @return the diffMP
	 */
	public BigDecimal getDiffMP() {
		return diffMP;
	}

	/**
	 * @param pDiffMP
	 *            the diffMP to set
	 */
	public void setDiffMP(BigDecimal pDiffMP) {
		BigDecimal oldValue = this.diffMP;
		this.diffMP = pDiffMP;
		firePropertyChange(diffMP_property, oldValue, pDiffMP);
	}

	/**
	 * @return the notSwitch
	 */
	public BigDecimal getNotSwitch() {
		return notSwitch;
	}

	/**
	 * @param pNotSwitch
	 *            the notSwitch to set
	 */
	public void setNotSwitch(BigDecimal pNotSwitch) {
		BigDecimal oldValue = this.notSwitch;
		this.notSwitch = pNotSwitch;
		firePropertyChange(notSwitch_property, oldValue, pNotSwitch);
	}

	/**
	 * @return the capitalOfStartEquityInPercent
	 */
	public BigDecimal getCapitalOfStartEquityInPercent() {
		return capitalOfStartEquityInPercent;
	}

	/**
	 * @param pCapitalOfStartEquityInPercent
	 *            the capitalOfStartEquityInPercent to set
	 */
	public void setCapitalOfStartEquityInPercent(BigDecimal pCapitalOfStartEquityInPercent) {
		BigDecimal oldValue = this.capitalOfStartEquityInPercent;
		this.capitalOfStartEquityInPercent = pCapitalOfStartEquityInPercent;
		firePropertyChange(capitalOfStartEquityInPercent_property, oldValue, pCapitalOfStartEquityInPercent);
	}

	/**
	 * @return the switchDistance
	 */
	public BigDecimal getSwitchDistance() {
		return this.switchDistance;
	}

	/**
	 * @param pSwitchDistance the switchDistance to set
	 */
	public void setSwitchDistance(BigDecimal pSwitchDistance) {
		this.switchDistance = pSwitchDistance;
		firePropertyChange(switchDistance_property, this.switchDistance, pSwitchDistance);
	}

	/**
	 * @return the startSwitch
	 */
	public BigDecimal getStartSwitch() {
		return this.startSwitch;
	}

	/**
	 * @param pStartSwitch the startSwitch to set
	 */
	public void setStartSwitch(BigDecimal pStartSwitch) {
		this.startSwitch = pStartSwitch;
		firePropertyChange(startSwitch_property, this.startSwitch, pStartSwitch);
	}

	/**
	 * @return the relation
	 */
	public BigDecimal getRelation() {
		return this.relation;
	}

	/**
	 * @param pRelation the relation to set
	 */
	public void setRelation(BigDecimal pRelation) {
		this.relation = pRelation;
		firePropertyChange(relation_property, this.relation, pRelation);
	}

	/**
	 * @return the diffMACD
	 */
	public BigDecimal getDiffMACD() {
		return this.diffMACD;
	}

	/**
	 * @param pDiffMACD the diffMACD to set
	 */
	public void setDiffMACD(BigDecimal pDiffMACD) {
		this.diffMACD = pDiffMACD;
		firePropertyChange(diffMACD_property, this.diffMACD, pDiffMACD);
	}

	/**
	 * @return the vorsprungHP
	 */
	public BigDecimal getVorsprungHP() {
		return this.vorsprungHP;
	}

	/**
	 * @param pVorsprungHP the vorsprungHP to set
	 */
	public void setVorsprungHP(BigDecimal pVorsprungHP) {
		this.vorsprungHP = pVorsprungHP;
		firePropertyChange(vorsprungHP_property, this.vorsprungHP, pVorsprungHP);
	}

	/**
	 * @return the startKontrakt
	 */
	public BigDecimal getStartKontrakt() {
		return this.startKontrakt;
	}

	/**
	 * @param pStartKontrakt the startKontrakt to set
	 */
	public void setStartKontrakt(BigDecimal pStartKontrakt) {
		this.startKontrakt = pStartKontrakt;
		firePropertyChange(startKontrakt_property, this.startKontrakt, pStartKontrakt);
	}

	/**
	 * @return the zukauf
	 */
	public BigDecimal getZukauf() {
		return this.zukauf;
	}

	/**
	 * @param pZukauf the zukauf to set
	 */
	public void setZukauf(BigDecimal pZukauf) {
		this.zukauf = pZukauf;
		firePropertyChange(zukauf_property, this.zukauf, pZukauf);
	}

	/**
	 * @return the gewinnMitnahme
	 */
	public BigDecimal getGewinnMitnahme() {
		return this.gewinnMitnahme;
	}

	/**
	 * @param pGewinnMitnahme the gewinnMitnahme to set
	 */
	public void setGewinnMitnahme(BigDecimal pGewinnMitnahme) {
		this.gewinnMitnahme = pGewinnMitnahme;
		firePropertyChange(gewinnMitnahme_property, this.gewinnMitnahme, pGewinnMitnahme);
	}

	/**
	 * @return the amplitudenMessung
	 */
	public BigDecimal getAmplitudenMessung() {
		return this.amplitudenMessung;
	}

	/**
	 * @param pAmplitudenMessung the amplitudenMessung to set
	 */
	public void setAmplitudenMessung(BigDecimal pAmplitudenMessung) {
		this.amplitudenMessung = pAmplitudenMessung;
		firePropertyChange(amplitudenMessung_property, this.amplitudenMessung, pAmplitudenMessung);
	}

	/**
	 * @return the randMessung
	 */
	public BigDecimal getRandMessung() {
		return this.randMessung;
	}

	/**
	 * @param pRandMessung the randMessung to set
	 */
	public void setRandMessung(BigDecimal pRandMessung) {
		this.randMessung = pRandMessung;
		firePropertyChange(randMessung_property, this.randMessung, pRandMessung);
	}

	/**
	 * @return the relGewZuk
	 */
	public BigDecimal getRelGewZuk() {
		return this.relGewZuk;
	}

	/**
	 * @param pRelGewZuk the relGewZuk to set
	 */
	public void setRelGewZuk(BigDecimal pRelGewZuk) {
		this.relGewZuk = pRelGewZuk;
		firePropertyChange(relGewZuk_property, this.relGewZuk, pRelGewZuk);
	}

	/**
	 * @return the umschichtung
	 */
	public BigDecimal getUmschichtung() {
		return this.umschichtung;
	}

	/**
	 * @param pUmschichtung the umschichtung to set
	 */
	public void setUmschichtung(BigDecimal pUmschichtung) {
		this.umschichtung = pUmschichtung;
		firePropertyChange(umschichtung_property, this.umschichtung, pUmschichtung);
	}

	/**
	 * @return the hoehenmessung
	 */
	public BigDecimal getHoehenmessung() {
		return this.hoehenmessung;
	}

	/**
	 * @param pHoehenmessung the hoehenmessung to set
	 */
	public void setHoehenmessung(BigDecimal pHoehenmessung) {
		this.hoehenmessung = pHoehenmessung;
		firePropertyChange(hoehenmessung_property, this.hoehenmessung, pHoehenmessung);
	}

	/**
	 * @return the zukaufVirtuell
	 */
	public BigDecimal getZukaufVirtuell() {
		return this.zukaufVirtuell;
	}

	/**
	 * @param pZukaufVirtuell the zukaufVirtuell to set
	 */
	public void setZukaufVirtuell(BigDecimal pZukaufVirtuell) {
		this.zukaufVirtuell = pZukaufVirtuell;
		firePropertyChange(zukaufVirtuell_property, this.zukaufVirtuell, pZukaufVirtuell);
	}


}
