/**
 * 
 */
package de.tm.za.gui;

import javax.swing.JFrame;

/**
 * Haupt Maske des Handelsprogramms. 
 * 
 * @author Elwashali
 * @created 13.01.2016
 * 
 */ 
public class MainFrame extends JFrame {

	/** serial version UID **/
	private static final long serialVersionUID = 1521043839120648002L;
	private static JFrame instance;

	/**
	 * Konstruktor
	 * 
	 * @param pFrame
	 *            View der Eingabe Maske.
	 * @param pTitle
	 *            Titel der View.
	 */
	public MainFrame(JFrame pFrame, String pTitle) {
		super(pTitle);
		instance = pFrame;

	}
	
	/**
	 * Liefert aktuelle Instanz des {@link MainFrame}.
	 * 
	 * @return {@link MainFrame} aktuelle Instanz.
	 */
	public JFrame getInstance() {
		return instance;
	}
}
