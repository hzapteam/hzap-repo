/**
 * 
 */
package de.tm.za.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tm.za.dto.ZustandautomatDTO;

/**
 * Represented the input mask where the parameters will be inputted.
 * 
 * @author Elwashali
 * @created 12.01.2016
 * 
 */
public class ZustandsautomatView extends AbstractView {

	private ZustandsautomatPresentationModel model;
	private JFrame mainFrame;

	private JComboBox<String> markts;
	private JComboBox<String> kontoAs;
	private JComboBox<String> kontoBs;
	private JTextField startEquity;
	private JTextField mpDiff;
	private JTextField notSwitch;
	private JTextField kapitalStartInProzent;
	private JTextField switchAbstand;
	private JTextField startSwitch;
	private JTextField relation;
	private JTextField macdDiff;
	private JTextField vorsprungHauptpos;
	private JTextField startKontrakt;
	private JTextField zukauf;
	private JTextField gewinnmitnahme;
	private JTextField amplitudenMessung;
	private JTextField randMessung;
	private JTextField relGewZuk;
	private JTextField umschichtung;
	private JTextField hoehenmessung;
	private JTextField zukaufVertuell;
	private JButton start;
	private JButton stop;
	private JButton beenden;

	/**
	 * Constructor
	 * 
	 * @param pModel {@link ZustandsautomatPresentationModel}
	 * @param pMainFrame {@link MainFrame}
	 *
	 */
	public ZustandsautomatView(ZustandsautomatPresentationModel pModel, MainFrame pMainFrame) {
		this.model = pModel;
		this.mainFrame = pMainFrame;
		initComponents();
		initEventHandling();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tm.za.gui.AbstractView#buildPanel()
	 */
	@Override
	public JPanel buildPanel() {
		JPanel mainPanel = new JPanel(new BorderLayout());

		FormLayout formLayout = new FormLayout(
				"5dlu, p, 5dlu, p:g, 10dlu, p,5dlu, p:g, 10dlu, p, 5dlu, p:g, 5dlu",
				"5dlu, p, 5dlu, p, 5dlu, p, 5dlu, p, 5dlu, p,5dlu, p, 5dlu, p, 5dlu, p, 5dlu, p, 5dlu");
		DefaultFormBuilder builder = new DefaultFormBuilder(formLayout);
		CellConstraints cc = new CellConstraints();

		// Z2
		builder.addLabel("Markt", cc.xy(2, 2));
		builder.add(this.markts, cc.xy(4, 2));

		// Z4
		builder.addLabel("Konto A (Start als HP)", cc.xy(2, 4));
		builder.add(this.kontoAs, cc.xy(4, 4));
		builder.addLabel("Konto B (Start als GP)", cc.xy(6, 4));
		builder.add(this.kontoBs, cc.xy(8, 4));

		// Z6
		builder.addLabel("Start-Equity (USD)", cc.xy(2, 6));
		builder.add(this.startEquity, cc.xy(4, 6));
		builder.addLabel("MP-Differenz (1 auf 3)", cc.xy(6, 6));
		builder.add(this.mpDiff, cc.xy(8, 6));
		builder.addLabel("Not-Switch (Ticks)", cc.xy(10, 6));
		builder.add(this.notSwitch, cc.xy(12, 6));

		// Z8
		builder.addLabel("Kapital von Start-Equity (%)", cc.xy(2, 8));
		builder.add(this.kapitalStartInProzent, cc.xy(4, 8));
		builder.addLabel("Switch-Abstand (Ticks)", cc.xy(6, 8));
		builder.add(this.switchAbstand, cc.xy(8, 8));
		builder.addLabel("Start-Switch(Ticks)", cc.xy(10, 8));
		builder.add(this.startSwitch, cc.xy(12, 8));

		// Z10
		builder.addLabel("Relation (GP : HP)(%)", cc.xy(2, 10));
		builder.add(this.relation, cc.xy(4, 10));
		builder.addLabel("MACD-Differenz (Ticks)", cc.xy(6, 10));
		builder.add(this.macdDiff, cc.xy(8, 10));
		builder.addLabel("Hauptpos. Vorsprung ($)", cc.xy(10, 10));
		builder.add(this.vorsprungHauptpos, cc.xy(12, 10));

		// Z12
		builder.addLabel("Start-Kontrakt (%)", cc.xy(2, 12));
		builder.add(this.startKontrakt, cc.xy(4, 12));
		builder.addLabel("Zukauf (%)", cc.xy(6, 12));
		builder.add(this.zukauf, cc.xy(8, 12));
		builder.addLabel("Gewinnmitnahme (%)", cc.xy(10, 12));
		builder.add(this.gewinnmitnahme, cc.xy(12, 12));

		// Z14
		builder.addLabel("Amplituden-Messung", cc.xy(2, 14));
		builder.add(this.amplitudenMessung, cc.xy(4, 14));
		builder.addLabel("Rand-Messung", cc.xy(6, 14));
		builder.add(this.randMessung, cc.xy(8, 14));
		builder.addLabel("Rel-Gew-Zuk (%)", cc.xy(10, 14));
		builder.add(this.relGewZuk, cc.xy(12, 14));

		// Z16
		builder.addLabel("Umschichtugn", cc.xy(2, 16));
		builder.add(this.umschichtung, cc.xy(4, 16));
		builder.addLabel("Höhenmessung", cc.xy(6, 16));
		builder.add(this.hoehenmessung, cc.xy(8, 16));
		builder.addLabel("Zukauf Virtuell (%)", cc.xy(10, 16));
		builder.add(this.zukaufVertuell, cc.xy(12, 16));

		
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.add("Parameter", builder.getPanel());
		

		mainPanel.add(tabbedPane, BorderLayout.CENTER);
		mainPanel.add(buildBottonsPanel(), BorderLayout.SOUTH);
		mainPanel.setBorder(Borders.DLU7_BORDER);

		return mainPanel;
	}

	/**
	 * Builds the buttons together in a separate panel.
	 * @return {@link JPanel} with the buttons
	 */
	private Component buildBottonsPanel() {
		ButtonBarBuilder builder = ButtonBarBuilder.createLeftToRightBuilder();
		builder.addGridded(this.start);
		builder.addRelatedGap();
		builder.addGridded(this.stop);
		builder.addGlue();
		builder.addGridded(this.beenden);
		
		return builder.getPanel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tm.za.gui.AbstractView#initComponents()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void initComponents() {

		List<String> markList = new ArrayList<String>();
		markList.add("Dax");
		SelectionInList<String> selectionInList = new SelectionInList<String>(markList);
		this.markts = BasicComponentFactory.createComboBox(selectionInList);

		this.kontoAs = BasicComponentFactory.createComboBox(new SelectionInList<>());
		this.kontoBs = BasicComponentFactory.createComboBox(new SelectionInList<>());

		this.startEquity = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.startEquity_property),
				new NumberFormatter());
		this.mpDiff = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.diffMP_property),
				new NumberFormatter());
		this.notSwitch = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.notSwitch_property),
				new NumberFormatter());
		this.kapitalStartInProzent = BasicComponentFactory.createFormattedTextField(
				this.model
						.getBufferedModel(ZustandautomatDTO.capitalOfStartEquityInPercent_property),
				new NumberFormatter());
		this.switchAbstand = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.switchDistance_property),
				new NumberFormatter());
		this.startSwitch = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.startSwitch_property),
				new NumberFormatter());
		this.relation = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.relation_property),
				new NumberFormatter());
		this.macdDiff = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.diffMACD_property),
				new NumberFormatter());
		this.vorsprungHauptpos = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.vorsprungHP_property),
				new NumberFormatter());
		this.startKontrakt = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.startKontrakt_property),
				new NumberFormatter());
		this.zukauf = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.zukauf_property),
				new NumberFormatter());
		this.gewinnmitnahme = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.gewinnMitnahme_property),
				new NumberFormatter());
		this.amplitudenMessung = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.amplitudenMessung_property),
				new NumberFormatter());
		this.randMessung = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.randMessung_property),
				new NumberFormatter());
		this.relGewZuk = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.relGewZuk_property),
				new NumberFormatter());
		this.umschichtung = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.umschichtung_property),
				new NumberFormatter());
		this.hoehenmessung = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.hoehenmessung_property),
				new NumberFormatter());
		this.zukaufVertuell = BasicComponentFactory.createFormattedTextField(
				this.model.getBufferedModel(ZustandautomatDTO.zukaufVirtuell_property),
				new NumberFormatter());
		this.start = new JButton("Start");
		this.stop = new JButton("Stop");
		this.beenden = new JButton("Beenden");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tm.za.gui.AbstractView#initEventHandling()
	 */
	@Override
	public void initEventHandling() {
		this.start.addActionListener(this.model.fetchStartAction());
		this.stop.addActionListener(this.model.fetchStopAction());
		this.beenden.addActionListener(this.model.fetchBeendenAction(this.mainFrame));
	}

}
