/**
 * 
 */
package de.tm.za.gui;

import javax.swing.JPanel;

/**
 * @author Elwashali
 * @created 12.01.2016
 * 
 */
public abstract class AbstractView {

	/**
	 * builds the components together in a panel.
	 * @return {@link JPanel}
	 */
	public abstract JPanel buildPanel();
	/**
	 * initializes the components of the View.
	 */
	public abstract void initComponents();
	/**
	 * initializes the event handling for the View.
	 */
	public abstract void initEventHandling();

}
