/**
 *
 */
package de.tm.za.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import com.jgoodies.binding.PresentationModel;

import de.tm.za.cvs.CsvFileReader;
import de.tm.za.dto.ZustandautomatDTO;
import de.tm.za.evaluation.TmpHmpCalculation;

/**
 * @author Elwashali
 * @created 12.01.2016
 *
 */
public class ZustandsautomatPresentationModel extends PresentationModel<ZustandautomatDTO> {

	/** serial version UID **/
	private static final long serialVersionUID = 8852026224894889783L;

	/**
	 * Constructor
	 *
	 * @param pBean
	 *            {@link ZustandautomatDTO}
	 */
	public ZustandsautomatPresentationModel(ZustandautomatDTO pBean) {
		super(pBean);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public ActionListener fetchStartAction() {

		return new AbstractStartAction();
	}

	public class AbstractStartAction extends AbstractAction {

		/** serial Version UID **/
		private static final long serialVersionUID = 446249308118865923L;

		/*
		 * (non-Javadoc)
		 *
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event. ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent pE) {
			// CSV auslesen
			// String path = "C:\\Users\\Administrator\\Desktop\\Pr�fung\\input.csv";
			String path = "C:\\Users\\Abdullatif\\Desktop\\Pr�fung\\input.csv";

			File csvFile = new File(path);
			CsvFileReader reader;
			TmpHmpCalculation tmpHmpCalculation = null;
			try {
				reader = new CsvFileReader(csvFile);
				reader.readCsvAndsetData();
				tmpHmpCalculation = new TmpHmpCalculation(reader.getMatrixData(), true, false);
				boolean finish = tmpHmpCalculation.readMatrix(tmpHmpCalculation.getMatrixData());
				Thread.sleep(100);
				if (finish) {
					tmpHmpCalculation = new TmpHmpCalculation(reader.getMatrixData(), false, true);
					finish = tmpHmpCalculation.readMatrix(tmpHmpCalculation.getMatrixData());
					Thread.sleep(100);
				}
			} catch (IOException | InterruptedException pE2) {
				pE2.printStackTrace();
			}

		}
	}

	/**
	 * @return
	 */
	public ActionListener fetchStopAction() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param pMainFrame
	 * @return
	 */
	public ActionListener fetchBeendenAction(JFrame pMainFrame) {
		// TODO Auto-generated method stub
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent pE) {
				pMainFrame.dispose();

			}
		};
	}

}
