/**
 *
 */
package de.tm.za.cvs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;


/**
 * Represent the csv file reader.
 *
 * @author Elwashali
 * @created 18.01.2016
 *
 */
public class CsvFileReader {

	private static final String SEPARATOR = ";";

	private final BufferedReader bufferedReader;
	private List<String[]> matrixData;

	/**
	 * Constructor
	 *
	 * @throws FileNotFoundException
	 *
	 */
	public CsvFileReader(File pFile) throws FileNotFoundException {
		bufferedReader = new BufferedReader(new FileReader(pFile));
		matrixData = new ArrayList<String[]>();

	}

	public void readCsvAndsetData() throws IOException, InterruptedException {

		String line = (bufferedReader.readLine());
		while ( line != null) {
				String[] values = StringUtils.delimitedListToStringArray(line, SEPARATOR);

				matrixData.add(values);
				line= bufferedReader.readLine();

			}

//		ausgeben(this.matrixData);
		Thread.sleep(500);

	}

	/**
	 * To get a value from the matrix with help the index of the row and the column
	 * @param pIndexColumn index of the column
	 * @param indexRow index of the row
	 * @return a value in String format
	 */
	public String getValue(int pIndexColumn, int indexRow) {

		String[] row = matrixData.get(indexRow);
		return row[pIndexColumn];
	}

	/**
	 * gibt die Matrix aus.
	 * @param pMatrics
	 */
	private void ausgeben(List<String[]> pMatrics) {
		int zIndex = 1;
		for( String[] zeile: pMatrics) {
			System.out.println();

			for(int i=0; i<zeile.length; i++) {
				String value = "";
				if(i== 0) {
					value = String.valueOf(zIndex);
				}
				 value += "\t"+zeile[i]+"("+i+")";
				System.out.print(value+" ");
			}
			zIndex++;
		}
		System.out.println("==================================");
	}

	/**
	 * @return the matrixData
	 */
	public List<String[]> getMatrixData() {
		return matrixData;
	}

}
